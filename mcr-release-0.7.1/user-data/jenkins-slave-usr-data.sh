#!/bin/bash
HostedZoneId=Z3BW6HW0YEZDDG
S3_BUCKET_NAME=cloud-fromation-user-data
PrivateDomainName=webservices.com
REGION=eu-west-1
if [[ -n ${S3_BUCKET_NAME} &&  -n ${PrivateDomainName} && -n ${REGION} ]]; then
aws s3 --no-verify-ssl cp s3://${S3_BUCKET_NAME}/user-data/common.sh /root/common.sh --region ${REGION}
else
echo "please provide Variables Value for S3_BUCKET_NAME, HttpProxyIporDNS,PrivateDomainName,REGION"
fi
if [[ -f "/root/common.sh" ]]; then
source "/root/common.sh"
else
echo "common libs does not exist"
exit 1
fi
if [[ -n ${HostedZoneId} && -n ${AWS_INSTANCEIP} && -n ${PrivateDomainName} && -n ${AWS_AZ} ]]; then
  CreateDnsARecord ${HostedZoneId} "/home/ubuntu/change.json" "jenkins-slave-${AWS_AZ}.${PrivateDomainName}" "${AWS_INSTANCEIP}"
fi
