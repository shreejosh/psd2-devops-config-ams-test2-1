def checkString(value) {
    if ( value != null && !value.isEmpty() && !value.trim().isEmpty() && value != "null"){
        return true
    }else {
        return false
    }
}
pipeline {
    agent { node { label 'PSD2-DEV-SLAVE' } }
    parameters {
        string(name: 'psd2AdapterConfigRepo', defaultValue: 'git@bitbucket.org:cap-gemini/zpsd2-adapter-config-pi6.git', description: 'psd2 adapter config repo')
        string(name: 'psd2BoiDevConfigRepo', defaultValue: 'git@bitbucket.org:cap-gemini/psd2-config-application-boidev-mcr-0.7.1.git', description: 'psd2 config repo for BoiDev ')
        string(name: 'psd2BoiTestConfigRepo', defaultValue: 'git@bitbucket.org:cap-gemini/psd2-config-application-boitest-mcr-0.7.1.git', description: 'psd2 config Repo for BoiTest')
        string(name: 'psd2BoiSITConfigRepo', defaultValue: 'git@bitbucket.org:cap-gemini/psd2-config-application-boisit-mcr-0.7.1.git', description: 'psd2 config Repo for BoiTest')
        string(name: 'gitCredentialsId', defaultValue: '56f76e32-4bd0-4cc8-a95b-cba3a21c9a18', description: 'Credentials')
        string(name: 'excludeDir', defaultValue: '', description: 'use coma(,) for multiple folder and file')
        string(name: 'msg', defaultValue: '"Committing Version"',description: 'git commit msg' )
        string(name: 'branch',  description: 'branch name adapter and product code')
        string(name: 'releaseBranch',  description: 'get code for release repo using branch name')
    }
    stages {
        stage ('delete workspace'){
            steps {
                    deleteDir()
                }
        }
        stage ('check branch exist') {
            when {
                expression {
                    checkString(params.branch) != true && checkString(params.releaseBranch) != true
                }
            }
            steps {
                script {
                    error "please provide me branch name"
                }
            }
        }
        stage ('git clone adapter'){
            steps {
                sh "mkdir adapter"
                dir ('adapter'){
                    git branch: "${params.branch}", credentialsId: "${params.gitCredentialsId}", url: "${params.psd2AdapterConfigRepo}"
                }
            }
        }
        stage ('git clone BoiDev release repo and remove existing dir'){
            steps {
                sh "mkdir release-BoiDev"
                dir ('release-BoiDev'){
                    git branch: "${params.releaseBranch}", credentialsId: "${params.gitCredentialsId}", url: "${params.psd2BoiDevConfigRepo}"
                    sh "ls  | grep -v .git |  xargs rm -rf"
                }
            }
        }

        stage ("copy file from adapter to BoiDev realase"){
            steps {
                script {
                    sh "echo  \".git\" > ignoreDirectoryAdapter"
                    if ( ! "${params.excludeDir}".isEmpty()) {
                        def ignoreDirectory = "${params.excludeDir}".trim().split(",")
                        for (int i = 0; i < ignoreDirectory.length; i++) {
                            sh "echo  \"${ignoreDirectory[i]}\" >> ignoreDirectoryAdapter"
                        }
                    }
                    sh "ls adapter/ | grep -v -f ignoreDirectoryAdapter | grep boiDev | xargs -I{} cp -r adapter/{} release-BoiDev/"
                }
            }
        }
        stage ("git push to BoiDev release repo"){
            steps {
                script {
                    dir ('release-BoiDev'){
                        def modifiesFile = sh(script: 'git status -s | grep  "^ M" | wc -l', returnStdout: true).trim()
                        def newAddFile = sh(script: 'git status -s | grep  "??" | wc -l', returnStdout: true).trim()
                        if (modifiesFile.toInteger() >= 1 || newAddFile.toInteger() >= 1 ){
                            sshagent (["${params.gitCredentialsId}"]){
                                sh "git add ."
                                sh "git commit -m ${params.msg}"
                                sh "git push origin ${params.releaseBranch}"
                            }
                        }
                    }
                }
            }
        }

        stage ('git clone BoiTest release repo and remove existing dir'){
            steps {
                sh "mkdir release-BoiTest"
                dir ('release-BoiTest'){
                    git branch: "${params.releaseBranch}", credentialsId: "${params.gitCredentialsId}", url: "${params.psd2BoiTestConfigRepo}"
                    sh "ls  | grep -v .git |  xargs rm -rf"
                }
            }
        }

        stage ("copy file from adapter to BoiTest realase"){
            steps {
                script {
                    sh "ls adapter/ | grep -v -f ignoreDirectoryAdapter | grep boiTest | xargs -I{} cp -r adapter/{} release-BoiTest/"
                }
            }
        }
        stage ("git push to BoiTest release repo"){
            steps {
                script {
                dir ('release-BoiTest'){
                    def modifiesFile = sh(script: 'git status -s | grep  "^ M" | wc -l', returnStdout: true).trim()
                    def newAddFile = sh(script: 'git status -s | grep  "??" | wc -l', returnStdout: true).trim()
                        if (modifiesFile.toInteger() >= 1 || newAddFile.toInteger() >= 1 ){
                            sshagent (["${params.gitCredentialsId}"]){
                                sh "git add ."
                                sh "git commit -m ${params.msg}"
                                sh "git push origin ${params.releaseBranch}"
                            }
                        }
                    }
                }
            }
        }
    stage ('git clone BoiSIT release repo and remove existing dir'){
            steps {
                sh "mkdir release-BoiSIT"
                dir ('release-BoiSIT'){
                    git branch: "${params.releaseBranch}", credentialsId: "${params.gitCredentialsId}", url: "${params.psd2BoiSITConfigRepo}"
                    sh "ls  | grep -v .git |  xargs rm -rf"
                }
            }
        }

        stage ("copy file from adapter to BoiSIT realase"){
            steps {
                script {
                    sh "ls adapter/ | grep -v -f ignoreDirectoryAdapter | grep boiSIT | xargs -I{} cp -r adapter/{} release-BoiSIT/"
                }
            }
        }
        stage ("git push to BoiSIT release repo"){
            steps {
                script {
                dir ('release-BoiSIT'){
                    def modifiesFile = sh(script: 'git status -s | grep  "^ M" | wc -l', returnStdout: true).trim()
                    def newAddFile = sh(script: 'git status -s | grep  "??" | wc -l', returnStdout: true).trim()
                        if (modifiesFile.toInteger() >= 1 || newAddFile.toInteger() >= 1 ){
                            sshagent (["${params.gitCredentialsId}"]){
                                sh "git add ."
                                sh "git commit -m ${params.msg}"
                                sh "git push origin ${params.releaseBranch}"
                            }
                        }
                    }
                }
            }
        }
    }
}
