pipeline {
    agent { node { label 'master' } }
    parameters {
        string(name: 'profile', defaultValue: 'oauth-server', description: 'profile Name for build Maven Project')
        choice(name: 'region',choices: 'eu-west-1\nus-east-1',description: ' Region for deploying Docker Image and Task definitions')
        string(name: 'entrypointPath', defaultValue: 'OAuthServer', description: 'archive Path for jar')
        choice(name: 'URL', choices: '843865943735.dkr.ecr.eu-west-1.amazonaws.com\n972530742661.dkr.ecr.us-east-1.amazonaws.com', description: 'register Docker to ECR Repo')
        string(name: 'others', defaultValue: 'boiDev', description: 'others')
        string(name: 'clusterName',defaultValue: "OauthServerCluster", description: 'ECS cluster Name')
        string(name: 'gitProjectRepo', defaultValue: 'git@bitbucket.org:cap-gemini/psd2-release-mcr-0.7.1.git\ngit@bitbucket.org:cap-gemini/psd2-release-mcr-0.7.1-nv.git', description: 'Eureka server repo')
        string(name: 'gitDevposConfigRepo', defaultValue: 'git@bitbucket.org:cap-gemini/psd2-devops-config.git', description: 'devpos config repo')
        string(name: 'gitCredentialsId', defaultValue: 'a5f0ddf7-8163-4174-8593-2032a80f6d9a', description: 'Credentials')
        string(name: 'keyStoreName', defaultValue: 'securitykeystore.jks', description: 'keystore name')
        string(name: 'gitBoiCredentialsId', defaultValue: 'a5f0ddf7-8163-4174-8593-2032a80f6d9a', description: 'Credentials')
        string(name: 'releaseName', defaultValue: 'mcr-release-0.7.1', description: 'Release Name')
        string(name: 'cipherBucketName', defaultValue: 'psd2-encrypted-data-boi-dev\nboi-api-platform-encrypted-data-mayrel', description: 'Release Name')
    }

    stages {
        stage ('clean workspace'){
            steps {
                deleteDir()
            }
        }
        stage ('git clone project'){
            steps {
                git branch: 'master', credentialsId: "${params.gitCredentialsId}", url: "${params.gitProjectRepo}"
            }
        }
        stage ('maven build jar'){
            steps {
                script {
                    def server = Artifactory.server('artifactory')
                    def buildInfo = Artifactory.newBuildInfo()
                    // maven build - plugin auto deploys the build artifact to the specifid repo
                    def rtMaven = Artifactory.newMavenBuild()
                    rtMaven.tool = 'm2'
                    rtMaven.deployer server: server, releaseRepo: 'libs-release-local', snapshotRepo: 'libs-snapshot-local'
                    dir('AdapterBuildAggregate'){
                       rtMaven.run pom: 'pom.xml', goals: 'clean install -P '+"${params.profile}" + ' sonar:sonar -Dsonar.branch='+"${params.profile}", buildInfo: buildInfo
                    }
                    sh "mkdir devops"
                    dir('devops'){
                        git branch: 'master', credentialsId: "${params.gitCredentialsId}", url: "${params.gitDevposConfigRepo}"
                    }
                    sh "mkdir ${env.WORKSPACE}/devops/${params.releaseName}/Docker/${params.profile}/classpath"
                    sh " find . -type f  -name *.jar ! -path \"*/.mvn/*\" ! -path \"*${params.entryPointPath}/*\"  | xargs -I{} cp -r {} ${env.WORKSPACE}/devops/${params.releaseName}/Docker/${params.profile}/classpath"
                    dir ('PSD2BuildAggregate'){
                       rtMaven.run pom: 'pom.xml', goals: 'clean install  -P '+"${params.profile}" + ' sonar:sonar -Dsonar.branch='+"${params.profile}", buildInfo: buildInfo
                    }
                    sh "cp ${params.entrypointPath}/target/*.jar ${env.WORKSPACE}/devops/${params.releaseName}/Docker/${params.profile}/"
                    server.publishBuildInfo buildInfo
                    }
                }
            }
        stage('git clone docker'){
            steps{
                dir('devops'){
                    git branch: 'master', credentialsId: "${params.gitCredentialsId}", url: "${params.gitDevposConfigRepo}"
                }
                sh "ls -al ${env.WORKSPACE}/devops/${params.releaseName}/Docker/${params.profile}"
                sh "cp ${params.entrypointPath}/target/*.jar ${env.WORKSPACE}/devops/${params.releaseName}/Docker/${params.profile}"
            }
        }
        stage('docker build'){
            steps{
                    dir("devops/${params.releaseName}/Docker/${params.profile}"){
                    script{
                    def dockerImageName = "${params.profile}".toLowerCase()
                    def awsLogin  = sh(script: "aws ecr get-login --region ${params.region}", returnStdout: true)
                    sh "${awsLogin}"
                    docker.build("${params.URL}/${dockerImageName}:v_${BUILD_NUMBER}","-f ${params.profile}-Dockerfile .").push()
                }
            }
            }
        }
    stage('create task definition'){
            steps{
                dir('devops'){
                script{
                    def taskDefinfitionParentPath=sh(script:"echo `pwd`", returnStdout: true)
                    if("${params.profile}"=='eureka-server' || "${params.profile}"=='config-server'){
                      sh "sed -e \"s;%BUILD_NUMBER%;${BUILD_NUMBER};g\" ${params.releaseName}/task-definitions/dev.json > ${params.profile}-v_${BUILD_NUMBER}.json"
                    }else{
                      sh "sed -e \"s;%BUILD_NUMBER%;${BUILD_NUMBER};g\" ${params.releaseName}/task-definitions/psd2apidev.json > ${params.profile}-v_${BUILD_NUMBER}.json"
                    }

                    if(fileExists("${params.profile}-v_${BUILD_NUMBER}.json")){
                        sh "cat ${params.profile}-v_${BUILD_NUMBER}.json"
                        echo "${taskDefinfitionParentPath}"

                        if ( ! "${params.keyStoreName}".isEmpty() ){
                            sh "sed -i \"s;keystore;${params.keyStoreName};g\" ${params.profile}-v_${BUILD_NUMBER}.json"
                        }
                        if ( ! "${params.releaseName}".isEmpty()){
                            sh "sed -i \"s;psd2-release;${params.releaseName};g\" ${params.profile}-v_${BUILD_NUMBER}.json"
                        }
                        if ( ! "${params.cipherBucketName}".isEmpty()){
                            sh "sed -i \"s;bucket_name;${params.cipherBucketName};g\" ${params.profile}-v_${BUILD_NUMBER}.json"
                        } else {
                            error "please profile cipherBucketName name"
                        }
                        sh "sed -i \"s;profile;${params.profile};g\" ${params.profile}-v_${BUILD_NUMBER}.json"
                        sh "sed -i \"s;userinput;${params.others};g\" ${params.profile}-v_${BUILD_NUMBER}.json"
                        sh "sed -i \"s;aws_region;${params.region};g\" ${params.profile}-v_${BUILD_NUMBER}.json"
                        sh "sed -i \"s;URL;${params.URL};g\" ${params.profile}-v_${BUILD_NUMBER}.json"
                        sh "cat ${params.profile}-v_${BUILD_NUMBER}.json"
                        sh "aws ecs register-task-definition --family ${params.profile} --cli-input-json file://${env.WORKSPACE}/devops/${params.profile}-v_${BUILD_NUMBER}.json --region ${params.region}"
                    }
                }
            }
            }

        }
    }
    post {
        always {
            archive "${params.entrypointPath}/target/*.jar"
            //deleteDir() /* clean up our workspace */
        }

        success {
            build job: 'mcr-0.7.1-Oauth-CD', parameters: [
                [$class: 'StringParameterValue',  name: 'clusterName', value: "${params.clusterName}"],
                [$class: 'StringParameterValue',  name: 'profile', value: "${params.profile}"],
                [$class: 'StringParameterValue',  name: 'URL', value: "${params.URL}"],
                [$class: 'StringParameterValue',  name: 'upstreamBuildNumber', value: "${env.BUILD_NUMBER}"],
                [$class: 'StringParameterValue',  name: 'region', value: "${params.region}"]]
        }
    }
}
