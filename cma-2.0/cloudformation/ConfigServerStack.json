{
	"AWSTemplateFormatVersion": "2010-09-09",
	"Description": "CloudFormation Template to provision the Config Instance",
	"Parameters": {
		"KeyName": {
			"Type": "AWS::EC2::KeyPair::KeyName",
			"Description": "Name of an existing EC2 KeyPair to enable SSH access to the bastion host",
			"MinLength": "1",
			"MaxLength": "64",
			"AllowedPattern": "[-_ a-zA-Z0-9]*",
			"ConstraintDescription": "can contain only alphanumeric characters, spaces, dashes and underscores."
		},
		"ProvisionNetScaler": {
			"Type": "String",
			"Default": "yes",
			"AllowedValues": ["yes", "no"],
			"Description": "To provision NetScaler or not"
		},
		"ProvisionProxy": {
			"Type": "String",
			"Default": "yes",
			"AllowedValues": ["yes", "no"],
			"Description": "To provision Proxy or not"
		},
		"ProvisionConfigUtility": {
			"Type": "String",
			"Default": "yes",
			"AllowedValues": ["yes", "no"],
			"Description": "To provision Proxy or not"
		},
		"NumberOfAZ": {
			"Type": "Number",
			"MinValue": "1",
			"MaxValue": "2",
			"Default": "2",
			"Description": "Number of Availabilty Zones"
		},
		"PrivateHostedZoneId": {
			"Type": "AWS::Route53::HostedZone::Id",
			"Description": "Private Hosted Zone Id for internal domains"
		},
		"PrivateDomainName": {
			"Description": "Domain Name of the project",
			"Type": "String",
			"Default": "webservices.com"
		},
		"MicroservicesSubnetAZ1": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Microservices Host Subnet for Availabilty Zone #1"
		},
		"MicroservicesSubnetAZ2": {
			"Type": "AWS::EC2::Subnet::Id",
			"Description": "Microservices Host Subnet for Availabilty Zone #2"
		},
		"Environment": {
			"Type": "String",
			"Description": "Environment to be provisioned",
			"Default": "dev",
			"AllowedValues": ["dev", "test", "sit", "uat", "preprod", "prod"]
		},
		"ClientName": {
			"Type": "String",
			"Description": "Client name"
		},
		"EncryptedTextS3Bucket": {
			"Type": "String",
			"Default": "psd2-encrypted-data-sandbox-release-boi-prod",
			"Description": "Encrypted S3 bucket"
		},
		"CloudFormationS3Bucket": {
			"Type": "String",
			"Default": "",
			"Description": "Encrypted S3 bucket"
		},
		"HttpProxyIporDNS": {
			"Description": "IP address or DNS name of the http proxy",
			"Type": "String"
		},
		"BastionSecurityGroup": {
			"Description": "SecurityGroup for bastion host",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"LBSecurityGroup": {
			"Description": "SecurityGroup for LB SecurityGroup",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"ConfigServerSecurityGroup": {
			"Description": "Security Groups for Mule Runtime servers",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"ConfigUtilitySecurityGroup": {
			"Description": "Security Groups for Mule Runtime servers",
			"Type": "AWS::EC2::SecurityGroup::Id"
		},
		"MicroservicesNACL": {
			"Type": "String",
			"Description": "Network ACL for UI Host"
		},
		"LBSubnetCidrBlockAZ1": {
			"Type": "String",
			"Description": "Cidr block of LBSubnetAZ1",
			"Default": "10.1.18.0/24",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"LBSubnetCidrBlockAZ2": {
			"Type": "String",
			"Description": "Cidr block of LBSubnetAZ2",
			"Default": "10.1.19.0/24",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"VPCCidrBlock": {
			"Type": "String",
			"Description": "Cidr block of the VPC",
			"Default": "10.1.16.0/20",
			"AllowedPattern": "(\\d{1,3}).(\\d{1,3}).(\\d{1,3}).(\\d{1,3})/(\\d{1,2})",
			"MaxLength": 18,
			"MinLength": 9
		},
		"ConfigServerTargetGroup": {
			"Type": "String",
			"Description": "ConfigServerTargetGroup ARN"
		},
		"BOISystemHealthMonitors": {
			"Description": "SNS Topic for Scale Up",
			"Type": "String"
		},
		"BOIEC2Monitors": {
			"Description": "SNS Topic for Scale Down",
			"Type": "String"
		},
		"BOIScaleUpMonitors": {
			"Description": "SNS Topic for Scale Up",
			"Type": "String"
		},
		"BOIScaleDownMonitors": {
			"Description": "SNS Topic for Scale Down",
			"Type": "String"
		}
	},
	"Mappings": {
		"RegionToAmiMapForECSAMI": {
			"us-east-1": {
				"ECSAmi": "ami-65bc7318"
			},
			"us-west-2": {
				"ECSAmi": "ami-18058860"
			},
			"eu-west-1": {
				"ECSAmi": "ami-18058860"
			},
			"eu-central-1": {
				"ECSAmi": "ami-b968bad6"
			}
		}
	},
	"Conditions": {
		"SecondAZUsed": {
			"Fn::Equals": [{
				"Ref": "NumberOfAZ"
			}, "2"]
		},
		"ProvisionConfigUtilityActivated": {
			"Fn::Equals": [{
				"Ref": "ProvisionConfigUtility"
			}, "yes"]
		},
		"ProvisionNetScalerActivated": {
			"Fn::Equals": [{
				"Ref": "ProvisionNetScaler"
			}, "yes"]
		}
	},
	"Resources": {
		"ConfigServerCluster": {
			"Type": "AWS::ECS::Cluster",
			"Properties": {
				"ClusterName": "ConfigServerCluster"
			}
		},
		"ConfigUtilityCluster": {
			"Type": "AWS::ECS::Cluster",
			"Condition": "ProvisionConfigUtilityActivated",
			"Properties": {
				"ClusterName": "ConfigUtilityCluster"
			}
		},
		"ConfigServerRepo": {
			"Type": "AWS::ECR::Repository",
			"Properties": {
				"RepositoryName": "config-server"
			}
		},
		"ConfigUtilityRepo": {
			"Type": "AWS::ECR::Repository",
			"Condition": "ProvisionConfigUtilityActivated",
			"Properties": {
				"RepositoryName": "config-utility"
			}
		},
		"LogsGroupForConfigServer": {
			"Type": "AWS::Logs::LogGroup",
			"Properties": {
				"LogGroupName": {
					"Fn::Join": ["", ["/var/log/ConfigServer_", {
							"Ref": "ClientName"
						},
						{
							"Ref": "Environment"
						}
					]]
				}
			}
		},
		"LogsGroupForConfigUtility": {
			"Type": "AWS::Logs::LogGroup",
			"Condition": "ProvisionConfigUtilityActivated",
			"Properties": {
				"LogGroupName": {
					"Fn::Join": ["", ["/var/log/ConfigUtility_", {
							"Ref": "ClientName"
						},
						{
							"Ref": "Environment"
						}
					]]
				}
			}
		},
		"FileSystem": {
			"Type": "AWS::EFS::FileSystem",
			"Properties": {
				"PerformanceMode": "generalPurpose",
				"FileSystemTags": [{
					"Key": "Name",
					"Value": "config-repo"
				}]
			}
		},
		"MountTargetAZ1": {
			"Type": "AWS::EFS::MountTarget",
			"Properties": {
				"FileSystemId": {
					"Ref": "FileSystem"
				},
				"SubnetId": {
					"Ref": "MicroservicesSubnetAZ1"
				},
				"SecurityGroups": [{
						"Ref": "ConfigServerSecurityGroup"
					},
					{
						"Fn::If": [
							"ProvisionConfigUtilityActivated",
							{
								"Ref": "ConfigUtilitySecurityGroup"
							},
							{
								"Ref": "AWS::NoValue"
							}
						]
					}
				]
			}
		},
		"MountTargetAZ2": {
			"Type": "AWS::EFS::MountTarget",
			"Properties": {
				"FileSystemId": {
					"Ref": "FileSystem"
				},
				"SubnetId": {
					"Ref": "MicroservicesSubnetAZ2"
				},
				"SecurityGroups": [{
						"Ref": "ConfigServerSecurityGroup"
					},
					{
						"Fn::If": [
							"ProvisionConfigUtilityActivated",
							{
								"Ref": "ConfigUtilitySecurityGroup"
							},
							{
								"Ref": "AWS::NoValue"
							}
						]
					}
				]
			}
		},
		"ConfigInstanceRole": {
			"Type": "AWS::IAM::Role",
			"Properties": {
				"AssumeRolePolicyDocument": {
					"Version": "2012-10-17",
					"Statement": [{
						"Effect": "Allow",
						"Principal": {
							"Service": [
								"ec2.amazonaws.com",
								"ssm.amazonaws.com"
							]
						},
						"Action": ["sts:AssumeRole"]
					}]
				},
				"Path": "/",
				"Policies": [{
						"PolicyName": "cloudwatch-log-agent-policy",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"logs:CreateLogGroup",
										"logs:CreateLogStream",
										"logs:PutLogEvents",
										"logs:DescribeLogStreams"
									],
									"Resource": [{
											"Fn::Join": ["", ["arn:aws:logs:", {
													"Ref": "AWS::Region"
												}, ":",
												{
													"Ref": "AWS::AccountId"
												}, ":log-group:/var/log/ConfigServer_",
												{
													"Ref": "ClientName"
												},
												{
													"Ref": "Environment"
												}, ":*"
											]]
										},
										{
											"Fn::If": [
												"ProvisionConfigUtilityActivated",
												{
													"Fn::Join": ["", ["arn:aws:logs:", {
															"Ref": "AWS::Region"
														}, ":",
														{
															"Ref": "AWS::AccountId"
														}, ":log-group:/var/log/ConfigUtility_",
														{
															"Ref": "ClientName"
														},
														{
															"Ref": "Environment"
														}, ":*"
													]]
												},
												{
													"Ref": "AWS::NoValue"
												}
											]
										},
										{
											"Fn::Join": ["", ["arn:aws:logs:", {
													"Ref": "AWS::Region"
												}, ":",
												{
													"Ref": "AWS::AccountId"
												}, ":log-group:AWS_ECS", ":*"
											]]
										}
									]
								},
								{
									"Effect": "Allow",
									"Action": [
										"ec2:DescribeTags"
									],
									"Resource": [
										"*"
									]
								}
							]
						}
					},
					{
						"PolicyName": "ssm-policy",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"ec2messages:GetMessages",
										"ec2messages:AcknowledgeMessage",
										"ec2messages:DeleteMessage",
										"ec2messages:FailMessage",
										"ec2messages:GetEndpoint",
										"ec2messages:SendReply",
										"ssm:DescribeInstanceProperties",
										"ssm:SendCommand",
										"ssm:GetDocument",
										"ssm:DescribeDocumentParameters"
									],
									"Resource": [
										"*"
									]
								},
								{
									"Effect": "Allow",
									"Action": [
										"ssm:UpdateInstanceInformation",
										"ssm:ListInstanceAssociations"
									],
									"Resource": [{
										"Fn::Join": [":", ["arn:aws:ec2", {
												"Ref": "AWS::Region"
											},
											{
												"Ref": "AWS::AccountId"
											}, "instance/*"
										]]
									}]
								},
								{
									"Effect": "Allow",
									"Action": "ssm:ListAssociations",
									"Resource": [{
										"Fn::Join": [":", ["arn:aws:ssm",
											{
												"Ref": "AWS::Region"
											},
											{
												"Ref": "AWS::AccountId"
											}, "*"
										]]
									}]
								}
							]
						}
					},
					{
						"PolicyName": "kms-decrypt",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"kms:Decrypt"
									],
									"Resource": [
										"*"
									]
								},
								{
									"Effect": "Allow",
									"Action": "s3:ListAllMyBuckets",
									"Resource": "arn:aws:s3:::*"
								},
								{
									"Effect": "Allow",
									"Action": [
										"s3:GetObject",
										"s3:ListBucket"
									],
									"Resource": [{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "EncryptedTextS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}]]
										},
										{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "EncryptedTextS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}, "/*"]]
										},
										{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "CloudFormationS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}]]
										},
										{
											"Fn::Join": ["", ["arn:aws:s3:::", {
												"Ref": "ClientName"
											}, "-", {
												"Ref": "CloudFormationS3Bucket"
											}, "-", {
												"Ref": "Environment"
											}, "/*"]]
										}
									]
								}
							]
						}
					},
					{
						"PolicyName": "ecs-access",
						"PolicyDocument": {
							"Version": "2012-10-17",
							"Statement": [{
									"Effect": "Allow",
									"Action": [
										"ecr:GetDownloadUrlForLayer",
										"ecr:BatchGetImage",
										"ecr:DescribeImages",
										"ecr:DescribeRepositories",
										"ecr:ListImages",
										"ecr:BatchCheckLayerAvailability",
										"ecr:GetRepositoryPolicy"
									],
									"Resource": [{
										"Fn::Join": ["", ["arn:aws:ecr:", {
												"Ref": "AWS::Region"
											}, ":",
											{
												"Ref": "AWS::AccountId"
											}, ":", "repository", "/config-server"
										]]
									},
									{
										"Fn::Join": ["", ["arn:aws:ecr:", {
												"Ref": "AWS::Region"
											}, ":",
											{
												"Ref": "AWS::AccountId"
											}, ":", "repository", "/config-utility"
										]]
									}]
								},
								{
									"Effect": "Allow",
									"Action": [
										"ecs:DeregisterContainerInstance",
										"ecs:DiscoverPollEndpoint",
										"ecs:Poll",
										"ecs:RegisterContainerInstance",
										"ecs:StartTelemetrySession",
										"ecs:Submit*",
										"ecr:GetAuthorizationToken"
									],
									"Resource": "*"
								}
							]
						}
					}
				]
			}
		},
		"InstanceProfileForConfigInstanceRole": {
			"Type": "AWS::IAM::InstanceProfile",
			"Properties": {
				"Roles": [{
					"Ref": "ConfigInstanceRole"
				}]
			}
		},
		"LaunchConfigForConfigServer": {
			"Type": "AWS::AutoScaling::LaunchConfiguration",
			"Metadata": {
				"Comment": "LaunchConfiguration template for Config server"
			},
			"Properties": {
				"KeyName": {
					"Ref": "KeyName"
				},
				"ImageId": {
					"Fn::FindInMap": ["RegionToAmiMapForECSAMI", {
						"Ref": "AWS::Region"
					}, "ECSAmi"]
				},
				"InstanceType": "t2.medium",
				"IamInstanceProfile": {
					"Ref": "InstanceProfileForConfigInstanceRole"
				},
				"UserData": {
					"Fn::Base64": {
						"Fn::Join": ["\n", [
							"#!/bin/bash",
							{
								"Fn::Join": [
									"", [
										"HostedZoneId=",
										{
											"Ref": "PrivateHostedZoneId"
										},
										"\n",
										"S3_BUCKET_NAME=",
										{
											"Ref": "ClientName"
										}, "-", {
											"Ref": "CloudFormationS3Bucket"
										}, "-", {
											"Ref": "Environment"
										},
										"\n",
										"HttpProxyIporDNS=",
										{
											"Ref": "HttpProxyIporDNS"
										},
										"\n",
										"PrivateDomainName=",
										{
											"Ref": "PrivateDomainName"
										},
										"\n",
										"REGION=",
										{
											"Ref": "AWS::Region"
										},
										"\n",
										"EFS_FILE_SYSTEM_ID=",
										{
											"Ref": "FileSystem"
										},
										"\n"
									]
								]
							},
							"export https_proxy=${HttpProxyIporDNS}",
							"if [[ -n ${S3_BUCKET_NAME} && -n ${HttpProxyIporDNS} &&  -n ${PrivateDomainName} && -n ${REGION} ]]; then",
							"aws s3 --no-verify-ssl cp s3://${S3_BUCKET_NAME}/user-data/common.sh /root/common.sh --region ${REGION}",
							"else",
							"echo \"please provide Variables Value for S3_BUCKET_NAME, HttpProxyIporDNS,PrivateDomainName,REGION\"",
							"fi",
							"if [[ -f \"/root/common.sh\" ]]; then",
							"source \"/root/common.sh\"",
							"else",
							"echo \"common libs does not exist\"",
							"exit 1",
							"fi",
							"ECS_CLUSTER_NAME=\"ConfigServerCluster\"",
							"if (( $?==0 )); then",
							"ProxySettings ${HttpProxyIporDNS} \"/etc/bashrc /etc/sysconfig/docker\"",
							"ProxySettingsForYum ${HttpProxyIporDNS}",
							"InstallPackage /home/ec2-user/aws_scripts/amazon-ssm-agent.rpm",
							"ProxySettingsforSSM ${HttpProxyIporDNS}",
							"StartOrStopService amazon-ssm-agent",
							"if [[ -n ${ECS_CLUSTER_NAME} ]]; then",
							"ReplaceOrAppendOnFile \"ECS_CLUSTER=\" \"${ECS_CLUSTER_NAME}\" \"/etc/ecs/ecs.config\"",
							"ProxySettingsForEcsAgent ${HttpProxyIporDNS}",
							"restartDocker ecs",
							"fi",
							"ConfigureEFS \"/mnt/efs\" \"${EFS_FILE_SYSTEM_ID}\"",
							"ConfigureAwsLogAgent ${HttpProxyIporDNS}",
							"fi",
							"sudo service chronyd start"
						]]
					}
				},
				"SecurityGroups": [{
					"Ref": "ConfigServerSecurityGroup"
				}]
			}
		},
		"AutoScalingGroupForConfigServer": {
			"Type": "AWS::AutoScaling::AutoScalingGroup",
			"Metadata": {
				"Comment": "Auto scaling group for Config server"
			},
			"Properties": {
				"DesiredCapacity": "2",
				"HealthCheckType": "EC2",
				"LaunchConfigurationName": {
					"Ref": "LaunchConfigForConfigServer"
				},
				"MaxSize": "2",
				"MetricsCollection": [{
					"Granularity": "1Minute",
					"Metrics": [
						"GroupDesiredCapacity"
					]
				}],
				"MinSize": "2",
				"TargetGroupARNs": [{
						"Ref": "ConfigServerTargetGroup"
					}

				],
				"NotificationConfigurations": [{
						"TopicARN": {
							"Ref": "BOIScaleUpMonitors"
						},
						"NotificationTypes": [
							"autoscaling:EC2_INSTANCE_LAUNCH",
							"autoscaling:EC2_INSTANCE_LAUNCH_ERROR"
						]
					},
					{
						"TopicARN": {
							"Ref": "BOIScaleDownMonitors"
						},
						"NotificationTypes": [
							"autoscaling:EC2_INSTANCE_TERMINATE",
							"autoscaling:EC2_INSTANCE_TERMINATE_ERROR"
						]
					}
				],
				"Tags": [{
					"Key": "Name",
					"Value": {
						"Fn::Join": ["", ["ConfigServer_", {
								"Ref": "ClientName"
							},
							{
								"Ref": "Environment"
							}
						]]
					},
					"PropagateAtLaunch": "true"
				}],
				"VPCZoneIdentifier": [{
						"Ref": "MicroservicesSubnetAZ1"
					},
					{
						"Ref": "MicroservicesSubnetAZ2"
					}
				]
			},
			"UpdatePolicy": {
				"AutoScalingScheduledAction": {
					"IgnoreUnmodifiedGroupSizeProperties": "true"
				},
				"AutoScalingRollingUpdate": {
					"MaxBatchSize": "1",
					"MinInstancesInService": "1",
					"PauseTime": "PT10M",
					"WaitOnResourceSignals": "false"
				}
			}
		},
		"LaunchConfigForConfigUtility": {
			"Type": "AWS::AutoScaling::LaunchConfiguration",
			"Condition": "ProvisionConfigUtilityActivated",
			"Metadata": {
				"Comment": "LaunchConfiguration template for Config Utility"
			},
			"Properties": {
				"KeyName": {
					"Ref": "KeyName"
				},
				"ImageId": {
					"Fn::FindInMap": ["RegionToAmiMapForECSAMI", {
						"Ref": "AWS::Region"
					}, "ECSAmi"]
				},
				"InstanceType": "t2.medium",
				"IamInstanceProfile": {
					"Ref": "InstanceProfileForConfigInstanceRole"
				},
				"UserData": {
					"Fn::Base64": {
						"Fn::Join": ["\n", [
							"#!/bin/bash",
							{
								"Fn::Join": [
									"", [
										"HostedZoneId=",
										{
											"Ref": "PrivateHostedZoneId"
										},
										"\n",
										"S3_BUCKET_NAME=",
										{
											"Ref": "ClientName"
										}, "-", {
											"Ref": "CloudFormationS3Bucket"
										}, "-", {
											"Ref": "Environment"
										},
										"\n",
										"HttpProxyIporDNS=",
										{
											"Ref": "HttpProxyIporDNS"
										},
										"\n",
										"PrivateDomainName=",
										{
											"Ref": "PrivateDomainName"
										},
										"\n",
										"REGION=",
										{
											"Ref": "AWS::Region"
										},
										"\n",
										"EFS_FILE_SYSTEM_ID=",
										{
											"Ref": "FileSystem"
										},
										"\n"
									]
								]
							},
							"export https_proxy=${HttpProxyIporDNS}",
							"if [[ -n ${S3_BUCKET_NAME} && -n ${HttpProxyIporDNS} &&  -n ${PrivateDomainName} && -n ${REGION} ]]; then",
							"aws s3 --no-verify-ssl cp s3://${S3_BUCKET_NAME}/user-data/common.sh /root/common.sh --region ${REGION}",
							"else",
							"echo \"please provide Variables Value for S3_BUCKET_NAME, HttpProxyIporDNS,PrivateDomainName,REGION\"",
							"fi",
							"if [[ -f \"/root/common.sh\" ]]; then",
							"source \"/root/common.sh\"",
							"else",
							"echo \"common libs does not exist\"",
							"exit 1",
							"fi",
							"ECS_CLUSTER_NAME=\"ConfigUtilityCluster\"",
							"if (( $?==0 )); then",
							"ProxySettings ${HttpProxyIporDNS} \"/etc/bashrc /etc/sysconfig/docker\"",
							"ProxySettingsForYum ${HttpProxyIporDNS}",
							"InstallPackage /home/ec2-user/aws_scripts/amazon-ssm-agent.rpm",
							"ProxySettingsforSSM ${HttpProxyIporDNS}",
							"StartOrStopService amazon-ssm-agent",
							"if [[ -n ${ECS_CLUSTER_NAME} ]]; then",
							"ReplaceOrAppendOnFile \"ECS_CLUSTER=\" \"${ECS_CLUSTER_NAME}\" \"/etc/ecs/ecs.config\"",
							"ProxySettingsForEcsAgent ${HttpProxyIporDNS}",
							"restartDocker ecs",
							"fi",
							"ConfigureEFS \"/mnt/efs\" \"${EFS_FILE_SYSTEM_ID}\"",
							"ConfigureAwsLogAgent ${HttpProxyIporDNS}",
							"fi",
							"sudo service chronyd start"
						]]
					}
				},
				"SecurityGroups": [{
					"Ref": "ConfigUtilitySecurityGroup"
				}]
			}
		},
		"AutoScalingGroupForConfigUtility": {
			"Type": "AWS::AutoScaling::AutoScalingGroup",
			"Condition": "ProvisionConfigUtilityActivated",
			"Metadata": {
				"Comment": "Auto scaling group for Config server"
			},
			"Properties": {
				"DesiredCapacity": "2",
				"HealthCheckType": "EC2",
				"LaunchConfigurationName": {
					"Ref": "LaunchConfigForConfigUtility"
				},
				"MaxSize": "2",
				"MetricsCollection": [{
					"Granularity": "1Minute",
					"Metrics": [
						"GroupDesiredCapacity"
					]
				}],
				"MinSize": "2",
				"NotificationConfigurations": [{
						"TopicARN": {
							"Ref": "BOIScaleUpMonitors"
						},
						"NotificationTypes": [
							"autoscaling:EC2_INSTANCE_LAUNCH",
							"autoscaling:EC2_INSTANCE_LAUNCH_ERROR"
						]
					},
					{
						"TopicARN": {
							"Ref": "BOIScaleDownMonitors"
						},
						"NotificationTypes": [
							"autoscaling:EC2_INSTANCE_TERMINATE",
							"autoscaling:EC2_INSTANCE_TERMINATE_ERROR"
						]
					}
				],
				"Tags": [{
					"Key": "Name",
					"Value": {
						"Fn::Join": ["", ["ConfigUtility_", {
								"Ref": "ClientName"
							},
							{
								"Ref": "Environment"
							}
						]]
					},
					"PropagateAtLaunch": "true"
				}],
				"VPCZoneIdentifier": [{
						"Ref": "MicroservicesSubnetAZ1"
					},
					{
						"Ref": "MicroservicesSubnetAZ2"
					}
				]
			},
			"UpdatePolicy": {
				"AutoScalingScheduledAction": {
					"IgnoreUnmodifiedGroupSizeProperties": "true"
				},
				"AutoScalingRollingUpdate": {
					"MaxBatchSize": "1",
					"MinInstancesInService": "1",
					"PauseTime": "PT10M",
					"WaitOnResourceSignals": "false"
				}
			}
		},
		"SSHBastionToConfigIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "22",
				"GroupId": {
					"Ref": "ConfigServerSecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "BastionSecurityGroup"
				},
				"ToPort": "22"
			}
		},
		"LBToConfigIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "8087",
				"GroupId": {
					"Ref": "ConfigServerSecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "LBSecurityGroup"
				},
				"ToPort": "8087"
			}
		},
		"NfsToConfigIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Properties": {
				"FromPort": "2049",
				"GroupId": {
					"Ref": "ConfigServerSecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "ConfigServerSecurityGroup"
				},
				"ToPort": "2049"
			}
		},
		"SSHBastionToConfigUtilityIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Condition": "ProvisionConfigUtilityActivated",
			"Properties": {
				"FromPort": "22",
				"GroupId": {
					"Ref": "ConfigUtilitySecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "BastionSecurityGroup"
				},
				"ToPort": "22"
			}
		},
		"LBToConfigUtilityIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Condition": "ProvisionConfigUtilityActivated",
			"Properties": {
				"FromPort": "6007",
				"GroupId": {
					"Ref": "ConfigUtilitySecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "LBSecurityGroup"
				},
				"ToPort": "6007"
			}
		},
		"NfsToConfigUtilityIngress": {
			"Type": "AWS::EC2::SecurityGroupIngress",
			"Condition": "ProvisionConfigUtilityActivated",
			"Properties": {
				"FromPort": "2049",
				"GroupId": {
					"Ref": "ConfigUtilitySecurityGroup"
				},
				"IpProtocol": "tcp",
				"SourceSecurityGroupId": {
					"Ref": "ConfigUtilitySecurityGroup"
				},
				"ToPort": "2049"
			}
		},
		"HTTPSAllowInboundNACLEntryFromLBAZ1ToConfigSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound VPC traffic From Config Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "MicroservicesNACL"
				},
				"RuleNumber": "150",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "LBSubnetCidrBlockAZ1"
				},
				"PortRange": {
					"From": "8087",
					"To": "8087"
				}
			}
		},
		"HTTPSAllowInboundNACLEntryFromLBAZ2ToConfigSubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Metadata": {
				"Comment": "Network ACL Entry inbound VPC traffic From Config Subnet"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "MicroservicesNACL"
				},
				"RuleNumber": "160",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "LBSubnetCidrBlockAZ2"
				},
				"PortRange": {
					"From": "8087",
					"To": "8087"
				}
			}
		},
		"HTTPSAllowInboundNACLEntryFromLBAZ1ToConfigUtilitySubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Condition": "ProvisionConfigUtilityActivated",
			"Metadata": {
				"Comment": "Network ACL Config Utility Entry inbound VPC traffic From Microservices Subnet #AZ1"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "MicroservicesNACL"
				},
				"RuleNumber": "170",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "LBSubnetCidrBlockAZ1"
				},
				"PortRange": {
					"From": "6007",
					"To": "6007"
				}
			}
		},
		"HTTPSAllowInboundNACLEntryFromLBAZ2ToConfigUtilitySubnet": {
			"Type": "AWS::EC2::NetworkAclEntry",
			"Condition": "ProvisionConfigUtilityActivated",
			"Metadata": {
				"Comment": "Network ACL Config Utility Entry inbound VPC traffic From Microservices Subnet #AZ2"
			},
			"Properties": {
				"NetworkAclId": {
					"Ref": "MicroservicesNACL"
				},
				"RuleNumber": "180",
				"Protocol": "6",
				"RuleAction": "allow",
				"Egress": "false",
				"CidrBlock": {
					"Ref": "LBSubnetCidrBlockAZ2"
				},
				"PortRange": {
					"From": "6007",
					"To": "6007"
				}
			}
		},
		"AlarmForInstanceStatusCheckOfConfigServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for AutoScalingGroupForConfigServer servers it goes active when the status check of any instance in ConfigServerCluster autoscaling group fails"
			},
			"Properties": {
				"AlarmActions": [{
					"Ref": "BOISystemHealthMonitors"
				}],
				"AlarmDescription": "Instance status check alarm for ConfigServerCluster server autoscaling group",
				"ComparisonOperator": "GreaterThanOrEqualToThreshold",
				"Dimensions": [{
					"Name": "AutoScalingGroupName",
					"Value": {
						"Ref": "AutoScalingGroupForConfigServer"
					}
				}],
				"EvaluationPeriods": "1",
				"MetricName": "StatusCheckFailed",
				"Namespace": "AWS/EC2",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "1",
				"Unit": "Count"
			}
		},
		"CPUHighAlarmForConfigServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConfigServerCluster servers it goes active when the CPU utilization is more than 75 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "CPU alarm for my ConfigServerCluster",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "ConfigServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		},
		"CPULowAlarmForConfigServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConfigServerCluster servers it goes active when the CPU Utilization is less than 25 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "CPU Low alarm for my ConfigServerCluster",
				"ComparisonOperator": "LessThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "ConfigServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "25"
			}
		},
		"MemoryHighAlarmForConfigServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConfigServerCluster servers it goes active when the Memory utilization is more than 75 percent for 5 consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "Memory High alarm for my ConfigServerCluster",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "ConfigServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "MemoryUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		},
		"MemoryLowAlarmForConfigServerCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConfigServerCluster servers it goes active when the Memory utilization is less than 25 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "Memory Low alarm for my ConfigServerCluster",
				"ComparisonOperator": "LessThanThreshold",
				"Dimensions":[{
					"Name": "ClusterName",
					"Value": "ConfigServerCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "MemoryUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "25"
			}
		},
		"AlarmForInstanceStatusCheckOfConfigUtilityCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for AutoScalingGroupForConfigUtility servers it goes active when the status check of any instance in ConfigUtilityCluster autoscaling group fails"
			},
			"Properties": {
				"AlarmActions": [{
					"Ref": "BOISystemHealthMonitors"
				}],
				"AlarmDescription": "Instance status check alarm for ConfigUtilityCluster server autoscaling group",
				"ComparisonOperator": "GreaterThanOrEqualToThreshold",
				"Dimensions": [{
					"Name": "AutoScalingGroupName",
					"Value": {
						"Ref": "AutoScalingGroupForConfigUtility"
					}
				}],
				"EvaluationPeriods": "1",
				"MetricName": "StatusCheckFailed",
				"Namespace": "AWS/EC2",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "1",
				"Unit": "Count"
			}
		},
		"CPUHighAlarmForConfigUtilityCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConfigUtilityCluster servers it goes active when the CPU utilization is more than 75 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "CPU alarm for my ConfigUtilityCluster",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "ConfigUtilityCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		},
		"CPULowAlarmForConfigUtilityCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConfigUtilityCluster servers it goes active when the CPU Utilization is less than 25 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "CPU Low alarm for my ConfigUtilityCluster",
				"ComparisonOperator": "LessThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "ConfigUtilityCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "CPUUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "25"
			}
		},
		"MemoryHighAlarmForConfigUtilityCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConfigUtilityCluster servers it goes active when the Memory utilization is more than 75 percent for 5 consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "Memory High alarm for my ConfigUtilityCluster",
				"ComparisonOperator": "GreaterThanThreshold",
				"Dimensions": [{
					"Name": "ClusterName",
					"Value": "ConfigUtilityCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "MemoryUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "75"
			}
		},
		"MemoryLowAlarmForConfigUtilityCluster": {
			"Type": "AWS::CloudWatch::Alarm",
			"Metadata": {
				"Comment": "Alarm for ConfigUtilityCluster servers it goes active when the Memory utilization is less than 25 percent for five consecutive minuites"
			},
			"Properties": {
				"AlarmActions": [{
						"Ref": "BOIEC2Monitors"
					}
				],
				"AlarmDescription": "Memory Low alarm for my ConfigUtilityCluster",
				"ComparisonOperator": "LessThanThreshold",
				"Dimensions":[{
					"Name": "ClusterName",
					"Value": "ConfigUtilityCluster"
				}],
				"EvaluationPeriods": "1",
				"MetricName": "MemoryUtilization",
				"Namespace": "AWS/ECS",
				"Period": "300",
				"Statistic": "Average",
				"Threshold": "25"
			}
		}
	}
}
