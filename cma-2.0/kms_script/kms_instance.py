import boto3
import base64
import json
import argparse
import os
import yaml
import sys
from botocore.exceptions import ClientError
class KMSInstance:
    """AWS Instance for Open """
    def __init__(self,resourceType,availabilityZone=None,aws_access_key_id=None,aws_secret_access_key=None):
        if not availabilityZone and aws_access_key_id and aws_secret_access_key:
            self.client = boto3.client(resourceType)
        elif availabilityZone and not aws_access_key_id and aws_secret_access_key:
            self.client = boto3.client(resourceType,availabilityZone)
        else:
            self.client = boto3.client(resourceType,availabilityZone, aws_access_key_id=aws_access_key_id,aws_secret_access_key=aws_secret_access_key)
            self.availabilityZone = availabilityZone
    def __ruturnNone__(self,responseStatus):
        if responseStatus is None:
            return True
        else:
            return False
    def create_cmk_key(self,origin,description):
        response = self.client.create_key(Origin=origin,Description=description)
        if response is not None:
            KeyId = response['KeyMetadata']['KeyId']
            KeyARN = response['KeyMetadata']['Arn']
            return { 'keyId' : KeyId, 'KeyARN': KeyARN }
        return self.__ruturnNone__(response)
    def create_key_alias(self,keyAlias,keyId):
        response=self.client.create_alias(AliasName=keyAlias,TargetKeyId=keyId)
        return response
    def create_data_key(self,keySpec,keyId):
        response=self.client.generate_data_key( KeyId=keyId,KeySpec=keySpec)
        return response
    def encrypt_data(self,keyId,plaintext):
        response = self.client.encrypt(KeyId=keyId,Plaintext=plaintext)
        return base64.b64encode(response['CiphertextBlob'])
    def decrypt_data(self,base64encrypted):
        CiphertextBlob = base64.b64decode(base64encrypted)
        response = self.client.decrypt(CiphertextBlob=CiphertextBlob)
        return response
    def check_cmk_keyid(self,keyId):
        try:
            response=self.client.describe_key(KeyId=keyId)
            if response['KeyMetadata']['Enabled']:
                return True
            else:
                return False
        except Exception as e:
                print e.message
                return False
    def download_file_multipart(self,filename,bucket_name,object):
        with open(filename, 'wb') as data:
            self.client.download_fileobj(bucket_name, object, data)
        return filename
    def upload_File(self,filename,bucket_name,object):
        with open(filename, 'rb') as data:
            self.client.upload_fileobj(data, bucket_name, object)
    def read_file(self, filename):
        with open(filename) as f:
            dataMap = yaml.load(f)
        dataMap = {} if dataMap is None else dataMap
        return dataMap
        return dataMap
    def create_yaml_file(self, filename,value):
        with open(filename, 'w') as data:
            yaml.dump(value, data, default_flow_style=False)
    def update_dict_value(self, data,dot,value):
        tree = current = data
        for key in dot:
            if key is dot[-1]:
                current[key]=value
            else:
                if not key in current:
                    current[key]={}
            current=current[key]
        return tree

def main():
    parser = argparse.ArgumentParser(description="KMS Automation to Create CMS Key")
    parser.add_argument('--region', type=str, help="reqion required", required=True)
    parser.add_argument('--aws_access_key_id', type=str, help="aws_access_key_id Required", required=False)
    parser.add_argument('--aws_secret_access_key', type=str, help="aws_secret_access_key Required", required=False)
    parser.add_argument('--origin', type=str, help="KeyName Required", required=False)
    parser.add_argument('--cmk_key_description', type=str, help="cmk key description required", required=False)
    parser.add_argument('--keyAlias', type=str, help="keyAlias  required for cmk key", required=False)
    parser.add_argument('--keyId', type=str, help="keyId to  required for cmk key", required=False)
    parser.add_argument('--plaintext', type=str, help="plain text password required", required=False)
    parser.add_argument('--plaintext_bucket_name', type=str, help="Path is required", required=False)
    parser.add_argument('--encrypted_bucket_name', type=str, help="Path is required", required=False)
    parser.add_argument('--releaseName', type=str, help="release name", required=False)
    parser.add_argument('--filename', type=str, help="added encrypted password required", required=False)
    parser.add_argument('--key', type=str, help="key where encrypted password assign as a value", required=False)
    aws = parser.parse_args()
    if aws.aws_access_key_id and aws.aws_secret_access_key is None:
        kmsInstance=KMSInstance(resourceType='kms',availabilityZone=aws.region)
        s3_client = KMSInstance(resourceType='s3',availabilityZone=aws.region)
    else:
        kmsInstance=KMSInstance(resourceType='kms',availabilityZone=aws.region,aws_access_key_id=aws.aws_access_key_id, aws_secret_access_key=aws.aws_secret_access_key)
        s3_client = KMSInstance(resourceType='s3',availabilityZone=aws.region,aws_access_key_id=aws.aws_access_key_id, aws_secret_access_key=aws.aws_secret_access_key)
    if aws.origin and aws.cmk_key_description and aws.keyAlias:
        response=kmsInstance.create_cmk_key(origin=aws.origin,description=aws.cmk_key_description)
        response_alias=kmsInstance.create_key_alias(aws.keyAlias,response['keyId'])
        print response
    elif aws.keyId and aws.plaintext:
        existingKeyId=kmsInstance.check_cmk_keyid(aws.keyId)
        if existingKeyId is True:
            encryptPassword = kmsInstance.encrypt_data(keyId=aws.keyId,plaintext=aws.plaintext)
            print encryptPassword
        elif existingKeyId is False:
            print "{0} is not valid".format(aws.keyId)
        if encryptPassword and aws.filename and aws.key and aws.plaintext_bucket_name and aws.encrypted_bucket_name:
            filename = aws.filename
            dot=aws.key.split('.')
            file = {}
            if filename not in file:
                file[filename]={}
                file[filename][aws.plaintext_bucket_name]={}
                file[filename][aws.encrypted_bucket_name]={}
            try:
                file[filename][aws.plaintext_bucket_name]['response']=s3_client.read_file(s3_client.download_file_multipart(aws.filename,aws.plaintext_bucket_name,aws.releaseName+"/"+aws.filename))
                file[filename][aws.plaintext_bucket_name]['password']=aws.plaintext
                file[filename][aws.encrypted_bucket_name]['response']=s3_client.read_file(s3_client.download_file_multipart(aws.filename,aws.encrypted_bucket_name,aws.releaseName+"/"+aws.filename))
                file[filename][aws.encrypted_bucket_name]['password']="{cipher}"+encryptPassword
                for bucket in file[filename]:
                    if isinstance(file[filename][bucket]['response'],dict):
                        file[filename][bucket]['response']=s3_client.update_dict_value(file[filename][bucket]['response'],dot,file[filename][bucket]['password'])
                        s3_client.create_yaml_file(filename,file[filename][bucket]['response'])
                        s3_client.upload_File(filename,bucket,aws.releaseName+"/"+filename)
                    elif file[filename][bucket]['response'] is None:
                        file[filename][bucket]['response']=s3_client.update_dict_value(file[filename][bucket]['response'],dot,file[filename][bucket]['password'])
                        s3_client.create_yaml_file(filename,file[filename][bucket]['response'])
                        s3_client.upload_File(filename,bucket,aws.releaseName+"/"+filename)
                    else:
                        print "please check your yaml file {0}".format(aws.filename)
            except Exception as e:
                sys.exit("ERROR {0} while creating new file with".format(e.message))
        else:
            sys.exit("ERROR {0} {1} not found".format(aws.filename,aws.key))
if __name__ == '__main__':
    main()

#python kms_instance.py --region eu-west-1  --aws_access_key_id  --aws_secret_access_key  --origin AWS_KMS --cmk_key_description "Bharat Script Testing"  --keyAlias alias/first_cm
